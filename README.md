# SAFETY-CAR APPLICATION  
**Java Project - *Team 10***

### Project Description  
**SAFETY CAR** is a web application, insurance-oriented for the end users. It allows simulation of amount and 
request insurance policies online. The application functionalities are related to three different type of users: 
public, private and administrators.  

The **public part** of application should be visible without authentication. It should provide the following functionalities: 
   - Simulate of car insurance offer based on some input criteria  
   - Creation of new account and log-in functionality  
   
The **private part** of application is for registered users only and should be accessible after successful login. The main 
functionalities provided for this area are:  
   - Send request for approval of offer (request policy)  
   - List history of user’s requests  
   
The **administrators** of the system should have permission to manage all major information objects in the system. 
The main functionalities provided for this area are:  
   - List of requests created in the system  
   - Approval or rejection of the requests  

### Functional Description  
In the following sections different functionalities will be described in more details.  
1) Create Account  
This functionality is accessible only from public part of the application. Implementation of page to register 
user in the system. The data to be entered are a valid email address as username and password.  

2) Simulate Offer  
This functionality is accessible from public and private part. Implementation of page – simulation form, 
where user should input specific parameters for its car and visualize the amount of expected total premium. Simulation 
form contain following business inputs:  
   - car (brand and model)  
   - cubic capacity  
   - first registration date  
   - driver age  
   - accidents in previous year (yes/no)  
     
    After click on simulate button, the expected premium is calculated and shown on the screen.  
When the total premium amount is calculated, the user has the possibility to request this offer for approval. 
If the user has not been logged in, the system should force him to log in. If the user is logged,
he automatically will be redirected to the page for policy request preparation.  

3) Request policy  
This functionality is accessible only from private part of the application. In order to issue a final policy, 
additional details are needed. A new page to be created in order to gather the following information:   
   - effective date of the policy  
   - attachment of image of vehicle registration certificate  
   - communication details: email, phone, postal address  
   
    All details from the simulated offer should be visible in the page. Once the policy is requested it should be 
stored in the system for further management. At that stage request becomes in state “pending” and should be 
treated by the administrators of the system.  

4) User’s Request History  
This functionality is accessible only from private part of the application. Implement a page to display list of 
all policy requests for the logged user with their details. The history of all requests to be shown 
(pending, approved and rejected) and chronologically sorted.  

5) Manage Requests  
This functionality is accessible only from administration part of the application. Implement a page to display list of 
all pending requests in the system with their details. The administrator should be able to accept or reject the request.  

### Technical Description  
***Database:***  
* The data of the application is stored in a relational database – MySQL/MariaDB.    

***Backend:***  
* JDK version 11
* Used tiered project structure (separate the application components in layers) 
* Used SpringMVC and SpringBoot framework 
* Used Hibernate in the persistence (repository) layer 
* Used Spring Security to handle user registration and user roles  

***Frontend:***  
* Used Spring MVC Framework with Thymeleaf template engine for generating the UI  
* Used AJAX for making asynchronous requests to the server where you find it appropriate  
* Applied own design and visual styles  
* Used Bootstrap  

### Deliverables
- **Link to the Trello Board:**  https://trello.com/b/YQee75Sw  
- **Link to Swagger:** http://localhost:8080/swagger-ui.html
