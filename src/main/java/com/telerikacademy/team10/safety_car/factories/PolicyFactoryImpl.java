package com.telerikacademy.team10.safety_car.factories;

import com.telerikacademy.team10.safety_car.exceptions.InvalidArgumentException;
import com.telerikacademy.team10.safety_car.models.Customer;
import com.telerikacademy.team10.safety_car.models.Policy;
import com.telerikacademy.team10.safety_car.models.PolicyRequest;
import com.telerikacademy.team10.safety_car.models.VehicleModel;
import com.telerikacademy.team10.safety_car.models.enums.StatusType;
import com.telerikacademy.team10.safety_car.repositories.contracts.CustomerRepository;
import com.telerikacademy.team10.safety_car.repositories.contracts.VehicleModelRepository;
import com.telerikacademy.team10.safety_car.services.contracts.FileService;
import com.telerikacademy.team10.safety_car.utility.DateUtility;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.Optional;

@Component
public class PolicyFactoryImpl implements PolicyFactory {

    final static String MODEL_MESSAGE = "Invalid input for vehicle model";
    final static String OWNER_MESSAGE = "Invalid input for policy owner";

    private final VehicleModelRepository modelRepository;
    private final CustomerRepository customerRepository;
    private final FileService fileService;

    public PolicyFactoryImpl(VehicleModelRepository modelRepository, CustomerRepository customerRepository, FileService fileService) {
        this.modelRepository = modelRepository;
        this.customerRepository = customerRepository;
        this.fileService = fileService;
    }

    @Override
    public Policy create(PolicyRequest request, String ownerId) {
        VehicleModel model = getValidModel(request.getModelId());
        Customer owner = getValidOwner(ownerId);
        String registrationCertificateLocation = getValidFileUrl(request.getRegistrationCertificate());

        return Policy.builder()
                .owner(owner)
                .effectiveDate(request.getEffectiveDate())
                .model(model)
                .registrationDate(request.getRegistrationDate())
                .cubicCapacity(request.getCubicCapacity())
                .hasPreviousAccidents(request.getHasPreviousAccidents())
                .registrationCertificate(registrationCertificateLocation)
                .driverAge(request.getDriverAge())
                .totalAmount(request.getTotalAmount())
                .policyNumber(request.getPolicyNumber())
                .email(request.getEmail())
                .phone(request.getPhone())
                .city(request.getCity())
                .street(request.getStreet())
                .status(StatusType.PENDING)
                .creationDate(DateUtility.convertToDate(LocalDate.now()))
                .build();
    }

    @NonNull
    private VehicleModel getValidModel(long modelId) {
        Optional<VehicleModel> model = modelRepository.findById(modelId);
        if (model.isEmpty()) {
            throw new InvalidArgumentException(MODEL_MESSAGE);
        }
        return model.get();
    }

    @NonNull
    private Customer getValidOwner(String ownerId) {
        Optional<Customer> customer = customerRepository.findById(ownerId);
        if (customer.isEmpty()) {
            throw new InvalidArgumentException(OWNER_MESSAGE);
        }
        return customer.get();
    }

    @NonNull
    private String getValidFileUrl(MultipartFile file) {
        return fileService.storeImage(file);
    }
}
