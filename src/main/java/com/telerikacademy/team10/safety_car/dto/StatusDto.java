package com.telerikacademy.team10.safety_car.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class StatusDto {
    @NotNull
    private String status;
}
