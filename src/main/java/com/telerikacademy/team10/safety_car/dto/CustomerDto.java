package com.telerikacademy.team10.safety_car.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
public class CustomerDto {

    private static final String EMAIL_LENGTH_MESSAGE = "Customer email should be between 10 and 50 symbols.";
    private static final String PASSWORD_LENGTH_MESSAGE = "Password should be between 5 and 50 symbols.";
    private static final String ADDRESS_LENGTH_MESSAGE = "Address should be between 3 and 50 symbols.";

    @Email
    @Size(min = 10, max = 50, message = EMAIL_LENGTH_MESSAGE)
    private String email;

    @Size(min = 5, max = 50, message = PASSWORD_LENGTH_MESSAGE)
    private String password;

    @Size(min = 5, max = 50, message = PASSWORD_LENGTH_MESSAGE)
    private String passwordConfirmation;
}