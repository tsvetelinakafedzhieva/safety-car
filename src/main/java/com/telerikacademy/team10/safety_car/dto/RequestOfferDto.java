package com.telerikacademy.team10.safety_car.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.util.Date;

@Data
public class RequestOfferDto {

    final static int MIN_CUBIC_CAPACITY = 1;
    final static int MAX_CUBIC_CAPACITY = 999999;
    final static String CUBIC_CAPACITY_MESSAGE = "Cubic capacity must be between 0 and 999999.";

    final static int MIN_DRIVER_AGE = 18;
    final static int MAX_DRIVER_AGE = 84;
    final static String DRIVER_AGE_MESSAGE = "Driver's age must be between 18 and 84.";


    @Positive
    private long modelId;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @PastOrPresent
    @NotNull
    private Date registrationDate;

    @Positive
    @Min(value = MIN_CUBIC_CAPACITY, message = CUBIC_CAPACITY_MESSAGE)
    @Max(value = MAX_CUBIC_CAPACITY, message = CUBIC_CAPACITY_MESSAGE)
    private int cubicCapacity;

    @Min(value = MIN_DRIVER_AGE, message = DRIVER_AGE_MESSAGE)
    @Max(value = MAX_DRIVER_AGE, message = DRIVER_AGE_MESSAGE)
    private int driverAge;

    @NotNull
    private Boolean hasPreviousAccidents;

}
