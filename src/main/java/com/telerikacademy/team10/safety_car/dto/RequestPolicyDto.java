package com.telerikacademy.team10.safety_car.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
public class RequestPolicyDto extends RequestPolicyDataDto {

    @NotNull
    private MultipartFile registrationCertificate;

}
