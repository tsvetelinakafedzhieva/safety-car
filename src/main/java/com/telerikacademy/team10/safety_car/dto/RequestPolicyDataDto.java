package com.telerikacademy.team10.safety_car.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.*;
import java.util.Date;

@Data
public class RequestPolicyDataDto {

    final static int MIN_LENGTH = 3;
    final static int MAX_LENGTH = 20;
    final static String CITY_MESSAGE = "City's name must be between 3 and 20 symbols long";
    final static String STREET_MESSAGE = "Street's name must be between 3 and 20 symbols long";

    final static int MIN_CUBIC_CAPACITY = 0;
    final static int MAX_CUBIC_CAPACITY = 999999;
    final static String CUBIC_CAPACITY_MESSAGE = "Cubic capacity must be between 0 and 999999.";

    final static int MIN_DRIVER_AGE = 18;
    final static int MAX_DRIVER_AGE = 84;
    final static String DRIVER_AGE_MESSAGE = "Driver's age must be between 18 and 84.";

    final static String PATTERN = "^\\+(?:[0-9]?){6,14}[0-9]$";

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Future
    private Date effectiveDate;

    @Positive
    private long modelId;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @PastOrPresent
    @NotNull
    private Date registrationDate;

    @Positive
    @Min(value = MIN_CUBIC_CAPACITY, message = CUBIC_CAPACITY_MESSAGE)
    @Max(value = MAX_CUBIC_CAPACITY, message = CUBIC_CAPACITY_MESSAGE)
    private int cubicCapacity;

    @NotNull
    private Boolean hasPreviousAccidents;


    @Min(value = MIN_DRIVER_AGE, message = DRIVER_AGE_MESSAGE)
    @Max(value = MAX_DRIVER_AGE, message = DRIVER_AGE_MESSAGE)
    private int driverAge;

    @Positive
    private double totalAmount;

    @Pattern(regexp = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$")
    private String email;

    @Pattern(regexp = PATTERN)
    private String phone;

    @Size(min = MIN_LENGTH, max = MAX_LENGTH, message = CITY_MESSAGE)
    private String city;

    @Size(min = MIN_LENGTH, max = MAX_LENGTH, message = STREET_MESSAGE)
    private String street;

    @NotNull
    private String policyNumber;
}
