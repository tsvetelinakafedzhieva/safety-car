package com.telerikacademy.team10.safety_car.mappers;

import com.telerikacademy.team10.safety_car.dto.RequestOfferDto;
import com.telerikacademy.team10.safety_car.dto.RequestPolicyDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OfferToPolicyMapper {

    RequestPolicyDto offerToPolicy (RequestOfferDto offerDto);

}
