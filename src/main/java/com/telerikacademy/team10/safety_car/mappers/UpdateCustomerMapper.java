package com.telerikacademy.team10.safety_car.mappers;

import com.telerikacademy.team10.safety_car.dto.UpdateCustomerDto;
import com.telerikacademy.team10.safety_car.models.Customer;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UpdateCustomerMapper {

    UpdateCustomerDto toDto(Customer source);

    Customer fromDto(UpdateCustomerDto destination);

}
