package com.telerikacademy.team10.safety_car.mappers;

import com.telerikacademy.team10.safety_car.dto.RequestOfferDto;
import com.telerikacademy.team10.safety_car.models.OfferRequest;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OfferDtoToOfferDomain {

    RequestOfferDto domainToDto(OfferRequest offerRequest);

    OfferRequest dtoToDomain(RequestOfferDto offerDto);
}
