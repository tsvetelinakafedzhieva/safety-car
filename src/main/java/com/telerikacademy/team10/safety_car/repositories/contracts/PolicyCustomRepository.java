package com.telerikacademy.team10.safety_car.repositories.contracts;

import com.telerikacademy.team10.safety_car.models.Customer;
import com.telerikacademy.team10.safety_car.models.Policy;
import com.telerikacademy.team10.safety_car.models.enums.FilterType;

import java.util.Optional;
import java.util.Set;

public interface PolicyCustomRepository {

    Set<Policy> getAllPolicies(Optional<Customer> customer, FilterType type, String key);

}
