package com.telerikacademy.team10.safety_car.repositories.contracts;

import com.telerikacademy.team10.safety_car.models.enums.RoleType;

import java.util.Optional;
import java.util.Set;

public interface AuthenticationRepository {

    boolean exists(String username);

    void create(String username, String password, RoleType type);

    void updatePassword(String oldPassword, String newPassword);

    void delete(String username);

    boolean isAuthenticated(String username);

    Optional<String> getPasswordFor(String username);

    Set<RoleType> getRoles(String username);

}
