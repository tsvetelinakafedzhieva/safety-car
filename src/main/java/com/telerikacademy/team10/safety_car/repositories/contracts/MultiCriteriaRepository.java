package com.telerikacademy.team10.safety_car.repositories.contracts;

import com.telerikacademy.team10.safety_car.models.MultiCriteria;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface MultiCriteriaRepository extends CrudRepository<MultiCriteria, Long> {

    @Query("select a " +
            "from MultiCriteria a " +
            "where :cubicCapacity between a.ccMin and a.ccMax " +
            "and :vehicleAge between a.carAgeMin and a.carAgeMax")
    Optional<MultiCriteria> getMultiCriteria(int cubicCapacity, int vehicleAge);

    @Override
    Set<MultiCriteria> findAll();

}
