package com.telerikacademy.team10.safety_car.repositories;

import com.telerikacademy.team10.safety_car.models.Customer;
import com.telerikacademy.team10.safety_car.models.Policy;
import com.telerikacademy.team10.safety_car.models.enums.FilterType;
import com.telerikacademy.team10.safety_car.models.enums.StatusType;
import com.telerikacademy.team10.safety_car.repositories.contracts.PolicyCustomRepository;
import com.telerikacademy.team10.safety_car.utility.DateUtility;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.*;

@Repository
public class PolicyRepositoryImpl implements PolicyCustomRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public PolicyRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Set<Policy> getAllPolicies(Optional<Customer> customer, FilterType type, String key) {
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Policy> criteriaQuery = criteriaBuilder.createQuery(Policy.class);
            Root<Policy> policyRoot = criteriaQuery.from(Policy.class);
            criteriaQuery = criteriaQuery.select(policyRoot);

            List<Predicate> wherePredicates;

            wherePredicates = customer.map(value -> getCustomerWherePredicates(value, criteriaBuilder, policyRoot, type, key))
                    .orElseGet(() -> getAgentWherePredicates(criteriaBuilder, policyRoot, type, key));

            criteriaQuery = criteriaQuery.orderBy(criteriaBuilder.asc(policyRoot.get("creationDate")));
            criteriaQuery = criteriaQuery.where(wherePredicates.toArray(Predicate[]::new));
            List<Policy> result = session.createQuery(criteriaQuery).getResultList();
            return new LinkedHashSet<>(result);
        }
    }


    private String like(String condition) {
        return String.format("%%%s%%", condition);
    }

    private List<Predicate> getCustomerWherePredicates(
            Customer customer,
            CriteriaBuilder criteriaBuilder,
            Root<Policy> policyRoot, FilterType type, String key) {

        List<Predicate> wherePredicates = new ArrayList<>();
        wherePredicates.add(criteriaBuilder.equal(policyRoot.get("owner"), customer));

        switch (type) {
            case DATE:
                Date date = DateUtility.convertToDate(LocalDate.parse(key));
                wherePredicates.add(criteriaBuilder.equal(policyRoot.get("creationDate"), date));
                break;
            case NUMBER:
                wherePredicates.add(criteriaBuilder.like(policyRoot.get("policyNumber"), like(key)));
                break;
            case STATUS:
                StatusType statusType = StatusType.statusTypeFromString(key);
                wherePredicates.add(criteriaBuilder.equal(policyRoot.get("status"), statusType));
            case ANY:
            default:
                break;
        }
        return wherePredicates;
    }

    private List<Predicate> getAgentWherePredicates(CriteriaBuilder criteriaBuilder,
                                                    Root<Policy> policyRoot,
                                                    FilterType type,
                                                    String key) {

        List<Predicate> wherePredicates = new ArrayList<>();
        wherePredicates.add(criteriaBuilder.equal(policyRoot.get("status"), StatusType.PENDING));

        switch (type) {
            case DATE:
                Date date = DateUtility.convertToDate(LocalDate.parse(key));
                wherePredicates.add(criteriaBuilder.equal(policyRoot.get("creationDate"), date));
                break;
            case NUMBER:
                wherePredicates.add(criteriaBuilder.like(policyRoot.get("policyNumber"), like(key)));
                break;
            case USERNAME:
                Path<Customer> customerPath = policyRoot.get("owner");
                wherePredicates.add(criteriaBuilder.like(customerPath.get("email"), like(key)));
            case ANY:
            default:
                break;
        }
        return wherePredicates;

    }

}
