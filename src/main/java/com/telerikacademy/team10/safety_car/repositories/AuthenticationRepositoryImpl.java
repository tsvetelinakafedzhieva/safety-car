package com.telerikacademy.team10.safety_car.repositories;

import com.telerikacademy.team10.safety_car.models.enums.RoleType;
import com.telerikacademy.team10.safety_car.repositories.contracts.AuthenticationRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class AuthenticationRepositoryImpl implements AuthenticationRepository {

    private final UserDetailsManager userDetailsManager;

    @Autowired
    public AuthenticationRepositoryImpl(UserDetailsManager userDetailsManager) {
        this.userDetailsManager = userDetailsManager;
    }

    @Override
    public boolean exists(String username) {
        return userDetailsManager.userExists(username);
    }

    @Override
    public void create(String username, String password, RoleType type) {
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(type.name());
        User user = new User(username, transformPassword(password), authorities);

        userDetailsManager.createUser(user);
    }

    @Override
    public void updatePassword(String oldPassword, String newPassword) {
        userDetailsManager.changePassword(oldPassword, newPassword);
    }

    @Override
    public void delete(String username) {
        userDetailsManager.deleteUser(username);
    }

    @Override
    public boolean isAuthenticated(String username) {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication != null && username.equals(authentication.getName());
    }

    @Override
    public Optional<String> getPasswordFor(String username) {
        final UserDetails user = userDetailsManager.loadUserByUsername(username);
        final String password = user.getPassword();
        return Optional.ofNullable(password);
    }

    @Override
    public Set<RoleType> getRoles(String username) {
        final UserDetails user = userDetailsManager.loadUserByUsername(username);
        return user.getAuthorities()
                .stream()
                .map(grantedAuthority -> RoleType.valueOf(grantedAuthority.getAuthority()))
                .collect(Collectors.toSet());
    }

    private String transformPassword(String password) {
        return String.format("{noop}%s", password);
    }
}
