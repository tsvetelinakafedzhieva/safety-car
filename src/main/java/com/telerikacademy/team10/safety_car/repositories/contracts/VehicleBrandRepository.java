package com.telerikacademy.team10.safety_car.repositories.contracts;

import com.telerikacademy.team10.safety_car.models.VehicleBrand;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface VehicleBrandRepository extends CrudRepository<VehicleBrand, Long> {

    @Override
    Set<VehicleBrand> findAll();
}
