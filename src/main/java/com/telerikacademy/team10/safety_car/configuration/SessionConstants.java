package com.telerikacademy.team10.safety_car.configuration;

public interface SessionConstants {
    String KEY_SESSION_POLICY_OFFER = "SessionConstants.KEY_SESSION_POLICY_OFFER";
    String KEY_SESSION_POST_LOGIN_DESTINATION = "SessionConstants.KEY_SESSION_POLICY_AUTH_DESTINATION";

}
