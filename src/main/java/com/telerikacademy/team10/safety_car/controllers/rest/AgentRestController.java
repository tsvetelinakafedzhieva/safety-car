package com.telerikacademy.team10.safety_car.controllers.rest;

import com.telerikacademy.team10.safety_car.models.MultiCriteria;
import com.telerikacademy.team10.safety_car.services.contracts.MultiCriteriaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Set;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/admin")
class AgentRestController {

    private final MultiCriteriaService multiCriteriaService;

    @Autowired
    AgentRestController(MultiCriteriaService multiCriteriaService) {
        this.multiCriteriaService = multiCriteriaService;
    }

    @PreAuthorize("hasRole('AGENT')")
    @GetMapping("/criteria")
    public Set<MultiCriteria> getCriteria() {
        return multiCriteriaService.getCurrentMultiCriteria();
    }

    @PreAuthorize("hasRole('AGENT')")
    @PutMapping("/criteria")
    public void putCriteria(@Valid @RequestPart("criteriaCSV") MultipartFile file) {
        multiCriteriaService.updateMultiCriteriaFromFile(file);
    }
}
