package com.telerikacademy.team10.safety_car.controllers.web;

import com.telerikacademy.team10.safety_car.dto.UpdateCustomerDto;
import com.telerikacademy.team10.safety_car.mappers.UpdateCustomerMapper;
import com.telerikacademy.team10.safety_car.models.Customer;
import com.telerikacademy.team10.safety_car.services.contracts.AuthenticationService;
import com.telerikacademy.team10.safety_car.services.contracts.CustomerService;
import com.telerikacademy.team10.safety_car.services.contracts.PolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.security.Principal;

@Controller
public class CustomerWebController {

    private final CustomerService customerService;
    private final UpdateCustomerMapper updateCustomerMapper;
    private final AuthenticationService authenticationService;
    private final PolicyService policyService;

    @Autowired
    public CustomerWebController(CustomerService customerService, UpdateCustomerMapper updateCustomerMapper, AuthenticationService authenticationService, PolicyService policyService) {
        this.customerService = customerService;
        this.updateCustomerMapper = updateCustomerMapper;
        this.authenticationService = authenticationService;
        this.policyService = policyService;
    }

    @PreAuthorize("hasRole('CUSTOMER')")
    @GetMapping("/profile")
    public String showProfile(Model model, Principal principal) {
        Customer customer = customerService.getByEmail(principal.getName());
        model.addAttribute("customer", customer);
        model.addAttribute("policies", customerService.getPolicies(customer.getEmail()));

        return "customer";
    }

    @PreAuthorize("hasRole('CUSTOMER')")
    @GetMapping("/profile/update")
    public String showUpdateCustomerPage(Principal principal,
                                         Model model) {

        Customer customer = customerService.getByEmail(principal.getName());
        model.addAttribute("customer", customer);
        model.addAttribute("updateCustomerDto", new UpdateCustomerDto());

        return "update-customer";
    }

    @PreAuthorize("hasRole('CUSTOMER')")
    @PostMapping("/profile/update")
    public String updateCustomer(@Valid @ModelAttribute UpdateCustomerDto updateCustomerDto,
                                 BindingResult result, Model model, Principal principal) {

        String email = principal.getName();

        if (result.hasErrors()) {
            model.addAttribute("updateCustomerDto", updateCustomerDto);
            model.addAttribute("error", "Email & password can't be empty!");
            return "update-customer";
        }

        if (!isNullOrEmpty(updateCustomerDto.getOldPassword())
                && !isNullOrEmpty(updateCustomerDto.getPassword())
                && !isNullOrEmpty(updateCustomerDto.getPasswordConfirmation())) {

            String oldPassword = String.format("{noop}%s", updateCustomerDto.getOldPassword());
            String newPassword = String.format("{noop}%s", updateCustomerDto.getPassword());
            String passwordConfirmation = String.format("{noop}%s", updateCustomerDto.getPasswordConfirmation());
            authenticationService.changePassword(email, oldPassword, newPassword, passwordConfirmation);
        }

        Customer customerToUpdate = updateCustomerMapper.fromDto(updateCustomerDto);
        customerService.update(email, customerToUpdate);
        return "redirect:/profile";
    }

    private boolean isNullOrEmpty(String text) {
        return text == null || text.isEmpty();
    }
}
