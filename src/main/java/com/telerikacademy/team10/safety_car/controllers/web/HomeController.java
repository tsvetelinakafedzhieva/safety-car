package com.telerikacademy.team10.safety_car.controllers.web;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @PreAuthorize("permitAll()")
    @GetMapping("/")
    public String showHomePage() {
        return "index";
    }

}
