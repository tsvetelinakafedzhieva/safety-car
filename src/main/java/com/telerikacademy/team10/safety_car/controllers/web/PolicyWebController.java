package com.telerikacademy.team10.safety_car.controllers.web;

import com.telerikacademy.team10.safety_car.configuration.SessionConstants;
import com.telerikacademy.team10.safety_car.dto.RequestOfferDto;
import com.telerikacademy.team10.safety_car.dto.RequestPolicyDto;
import com.telerikacademy.team10.safety_car.dto.StatusDto;
import com.telerikacademy.team10.safety_car.mappers.OfferDtoToOfferDomain;
import com.telerikacademy.team10.safety_car.mappers.OfferToPolicyMapper;
import com.telerikacademy.team10.safety_car.mappers.PolicyDtoToRequestPolicy;
import com.telerikacademy.team10.safety_car.models.*;
import com.telerikacademy.team10.safety_car.models.enums.StatusType;
import com.telerikacademy.team10.safety_car.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.validation.Validator;
import java.security.Principal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;
import java.util.Set;

@Controller
@RequestMapping
public class PolicyWebController {

    public static final String OFFER_MAPPING = "/offer";
    private static final String OFFER_TEMPLATE = "offer";

    private final MultiCriteriaService multiCriteriaService;
    private final VehicleService vehicleService;

    private final OfferDtoToOfferDomain offerMapper;

    private final Validator validator;

    private final OfferToPolicyMapper offerPolicyMapper;

    private final PolicyService policyService;

    private final PolicyDtoToRequestPolicy policyMapper;

    private final CustomerService customerService;

    @Autowired
    public PolicyWebController(MultiCriteriaService multiCriteriaService,
                               OfferDtoToOfferDomain offerMapper,
                               VehicleService vehicleService,
                               Validator validator,
                               OfferToPolicyMapper offerPolicyMapper,
                               PolicyService policyService,
                               PolicyDtoToRequestPolicy policyMapper,
                               CustomerService customerService) {
        this.multiCriteriaService = multiCriteriaService;
        this.offerMapper = offerMapper;
        this.vehicleService = vehicleService;
        this.validator = validator;
        this.offerPolicyMapper = offerPolicyMapper;
        this.policyService = policyService;
        this.policyMapper = policyMapper;
        this.customerService = customerService;
    }

    @PreAuthorize("permitAll()")
    @GetMapping(OFFER_MAPPING)
    public String showCalculateOffer(Model model, HttpSession session) {
        RequestOfferDto requestOfferDto = (RequestOfferDto) session
                .getAttribute(SessionConstants.KEY_SESSION_POLICY_OFFER);

        clearSession(session);

        if (requestOfferDto == null) {
            requestOfferDto = new RequestOfferDto();
        } else if (isValid(requestOfferDto)) {
            Premium total = calculateOffer(requestOfferDto);
            model.addAttribute("total", total.getTotalPremium());
        }
        model.addAttribute("dto", requestOfferDto);

        Set<VehicleBrand> vehicleBrands = vehicleService.getAllBrands();
        model.addAttribute("brands", vehicleBrands);

        return OFFER_TEMPLATE;
    }

    private void clearSession(@NonNull HttpSession session) {
        session.removeAttribute(SessionConstants.KEY_SESSION_POLICY_OFFER);
        session.removeAttribute(SessionConstants.KEY_SESSION_POST_LOGIN_DESTINATION);
    }

    private boolean isValid(@NonNull RequestOfferDto requestOfferDto) {
        Set<?> violations = validator.validate(requestOfferDto);
        return violations.isEmpty();
    }


    @PreAuthorize("permitAll()")
    @PostMapping(OFFER_MAPPING)
    public String calculateOffer(@Valid @ModelAttribute RequestOfferDto dto,
                                 BindingResult result,
                                 Model model) {
        Set<VehicleBrand> vehicleBrands = vehicleService.getAllBrands();
        model.addAttribute("dto", dto);
        model.addAttribute("brands", vehicleBrands);

        if (result.hasErrors()) {
            model.addAttribute("dto", dto);
            model.addAttribute("brands", vehicleBrands);
            return OFFER_TEMPLATE;
        }

        Premium total = calculateOffer(dto);
        model.addAttribute("total", total.getTotalPremium());

        return OFFER_TEMPLATE;
    }

    @PreAuthorize("hasRole('CUSTOMER')")
    @PostMapping("/policy")
    public String showRequestPolicy(@Valid @ModelAttribute RequestOfferDto offer,
                                    Model model, Principal principal) {
        Premium total = calculateOffer(offer);
        RequestPolicyDto policyDto = offerPolicyMapper.offerToPolicy(offer);
        policyDto.setTotalAmount(total.getTotalPremium());
        VehicleModel vehicleModel = vehicleService.getModelById(offer.getModelId());

        String currentCustomer = principal.getName();
        Customer customer = customerService.getByEmail(currentCustomer);
        policyDto.setCity(customer.getCity());
        policyDto.setStreet(customer.getStreet());
        policyDto.setPhone(customer.getPhone());
        policyDto.setEmail(customer.getEmail());

        String policyNumber = policyService.generatePolicyNumber(customer);
        policyDto.setPolicyNumber(policyNumber);

        Date tomorrow = getTomorrow();

        model.addAttribute("tomorrow", tomorrow);
        model.addAttribute("number", policyNumber);
        model.addAttribute("offer", offer);
        model.addAttribute("policyDto", policyDto);
        model.addAttribute("model", vehicleModel);
        model.addAttribute("total", String.format("%.2f", total.getTotalPremium()));

        return "policy";
    }

    @PreAuthorize("hasRole('CUSTOMER')")
    @PostMapping("/policy/create")
    public String sendRequestPolicy(@Valid @ModelAttribute RequestPolicyDto policy,
                                    BindingResult result,
                                    Principal principal) {
        if (result.hasErrors()) {
            return "error";
        }
        PolicyRequest policyRequest = policyMapper.dtoToEntity(policy);
        policyService.createPolicy(policyRequest, principal.getName());
        return "policy-message";
    }

    private Premium calculateOffer(RequestOfferDto requestOfferDto) {
        OfferRequest offerParams = offerMapper.dtoToDomain(requestOfferDto);
        return multiCriteriaService.calculateTotalPremium(offerParams);
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping("/policies")
    public String showAll(@RequestParam(required = false) String filterType,
                          @RequestParam(required = false) String filterKey,
                          Model model, Principal principal) {
        Set<Policy> policies = policyService.getPoliciesForUser(
                principal.getName(),
                Optional.ofNullable(filterType),
                Optional.ofNullable(filterKey)
        );
        model.addAttribute("policies", policies);
        return "policies";
    }


    @PreAuthorize("isAuthenticated()")
    @GetMapping("/policies/{id}")
    public String showPolicy(@PathVariable long id, Model model) {
        Policy policy = policyService.getById(id);
        model.addAttribute("statusDto", new StatusDto());
        model.addAttribute("policy", policy);
        return "get-policy";
    }

    @PreAuthorize("isAuthenticated()")
    @PostMapping("/policies/{id}")
    public String updateStatus(@PathVariable long id, @ModelAttribute StatusDto dto, Principal principal) {
        StatusType statusType = StatusType.statusTypeFromString(dto.getStatus());
        policyService.updatePolicyStatus(id, principal.getName(), statusType);

        return "redirect:/policies";
    }

    private Date getTomorrow() {
        return Date.from(LocalDateTime.now().plusDays(1).atZone(ZoneId.systemDefault()).toInstant());
    }

}
