package com.telerikacademy.team10.safety_car.controllers.web;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

@Controller
public class CustomErrorController implements ErrorController {

    @RequestMapping(path = "error")
    public String handleError(HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if (status != null) {
            int statusCode = Integer.parseInt(status.toString());

            if (statusCode == HttpStatus.UNAUTHORIZED.value()) {
                return "error-401";
            }
            if (statusCode == HttpStatus.NOT_FOUND.value()) {
                return "error-404";
            }
            if (statusCode == HttpStatus.PARTIAL_CONTENT.value()) {
                return "error-password";
            }
            if (statusCode == HttpStatus.ALREADY_REPORTED.value()) {
                return "error-email";
            }
        }
        return "error";
    }

    @Override
    public String getErrorPath() {
        return "error";
    }
}
