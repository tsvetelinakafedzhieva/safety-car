package com.telerikacademy.team10.safety_car.services;

import com.telerikacademy.team10.safety_car.exceptions.EntityNotFoundException;
import com.telerikacademy.team10.safety_car.models.VehicleBrand;
import com.telerikacademy.team10.safety_car.models.VehicleModel;
import com.telerikacademy.team10.safety_car.repositories.contracts.VehicleBrandRepository;
import com.telerikacademy.team10.safety_car.repositories.contracts.VehicleModelRepository;
import com.telerikacademy.team10.safety_car.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class VehicleServiceImpl implements VehicleService {

    private static final String MODEL_BAD_REQUEST = "Model with %d ID was not found";


    private final VehicleModelRepository modelRepository;
    private final VehicleBrandRepository brandRepository;

    @Autowired
    public VehicleServiceImpl(VehicleModelRepository modelRepository, VehicleBrandRepository brandRepository) {
        this.modelRepository = modelRepository;
        this.brandRepository = brandRepository;
    }

    @Override
    public Set<VehicleBrand> getAllBrands() {
        return brandRepository.findAll();
    }

    @Override
    public VehicleModel getModelById(long id) {
        Optional<VehicleModel> model = modelRepository.findById(id);
        if (model.isEmpty()) {
            throw new EntityNotFoundException(String.format((MODEL_BAD_REQUEST), id));
        }
        return model.get();
    }
}
