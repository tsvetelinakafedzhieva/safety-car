package com.telerikacademy.team10.safety_car.services;

import com.telerikacademy.team10.safety_car.controllers.rest.FileController;
import com.telerikacademy.team10.safety_car.exceptions.FileNotFoundException;
import com.telerikacademy.team10.safety_car.exceptions.FileStorageException;
import com.telerikacademy.team10.safety_car.exceptions.InvalidArgumentException;
import com.telerikacademy.team10.safety_car.repositories.contracts.FileRepository;
import com.telerikacademy.team10.safety_car.services.contracts.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Optional;

@Service
public class FileServiceImpl implements FileService {

    private final static String FILE_NOT_STORED = "Could not store file with name: %s";
    private final static String FILE_NOT_FOUND = "File not found: %s";

    private final FileRepository repository;

    @Autowired
    public FileServiceImpl(FileRepository repository) {
        this.repository = repository;
    }

    @Override
    public String storeImage(MultipartFile file) {
        String fileContentType = file.getContentType();
        if (fileContentType == null || !fileContentType.startsWith("image/")) {
            throw new InvalidArgumentException("Provided file must be some sort of image");
        }

        String originalFilename = file.getOriginalFilename();
        if (originalFilename == null) {
            throw new InvalidArgumentException("Provided file must have valid filename");
        }

        Optional<String> uploadedFileName = repository.saveFile(file);

        if (uploadedFileName.isEmpty()) {
            throw new FileStorageException(String.format(FILE_NOT_STORED, originalFilename));
        }

        return ServletUriComponentsBuilder.fromCurrentContextPath()
                .path(FileController.ENDPOINT_FILE_DISTRIBUTION)
                .path(uploadedFileName.get())
                .toUriString();
    }

    @Override
    public Resource loadFileAsResource(String fileName) {
        Optional<Resource> loadedFile = repository.getAsResource(fileName);

        if (loadedFile.isEmpty()) {
            throw new FileNotFoundException(String.format(FILE_NOT_FOUND, fileName));
        }

        return loadedFile.get();
    }


}
