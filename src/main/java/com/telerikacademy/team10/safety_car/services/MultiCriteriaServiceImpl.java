package com.telerikacademy.team10.safety_car.services;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.telerikacademy.team10.safety_car.exceptions.InvalidArgumentException;
import com.telerikacademy.team10.safety_car.models.MultiCriteria;
import com.telerikacademy.team10.safety_car.models.OfferRequest;
import com.telerikacademy.team10.safety_car.models.Premium;
import com.telerikacademy.team10.safety_car.repositories.contracts.MultiCriteriaRepository;
import com.telerikacademy.team10.safety_car.services.contracts.MultiCriteriaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
public class MultiCriteriaServiceImpl implements MultiCriteriaService {
    private final MultiCriteriaRepository multiCriteriaRepository;

    final static int MIN_DRIVER_AGE = 25;
    final static double DRIVER_AGE_COEFFICIENT = 1.05;
    final static double ACCIDENT_COEFFICIENT = 1.2;
    final static double TAX_COEFFICIENT = 0.1;

    final static String NOT_FOUND_MESSAGE = "Invalid parameters.";

    @Autowired
    public MultiCriteriaServiceImpl(MultiCriteriaRepository multiCriteriaRepository) {
        this.multiCriteriaRepository = multiCriteriaRepository;
    }

    @Override
    public Premium calculateTotalPremium(OfferRequest offerRequest) {

        int vehicleAge = calculateVehicleAge(offerRequest.getRegistrationDate());
        MultiCriteria multiCriteria = getMultiCriteria(offerRequest.getCubicCapacity(), vehicleAge);

        double netPremium = calculateNetPremium(multiCriteria.getBaseAmount(), offerRequest.isHasPreviousAccidents(), offerRequest.getDriverAge());
        double taxPremium = calculateTaxPremium(netPremium);
        double totalPremium = netPremium + taxPremium;

        return new Premium(totalPremium);
    }

    @Override
    @Transactional
    public void updateMultiCriteriaFromFile(MultipartFile file) {
        List<MultiCriteria> results;
        try {
            MappingIterator<MultiCriteria> mapper = new CsvMapper()
                    .readerWithTypedSchemaFor(MultiCriteria.class)
                    .readValues(file.getBytes());
            results = mapper.readAll();
        } catch (IOException e) {
            throw new InvalidArgumentException("Invalid file format");
        }
        multiCriteriaRepository.deleteAll();
        multiCriteriaRepository.saveAll(results);
    }

    @Override
    public Set<MultiCriteria> getCurrentMultiCriteria() {
        return multiCriteriaRepository.findAll();
    }

    private MultiCriteria getMultiCriteria(int cubicCapacity, int vehicleAge) {
        Optional<MultiCriteria> multiCriteria = multiCriteriaRepository.getMultiCriteria(cubicCapacity, vehicleAge);
        if (multiCriteria.isEmpty()) {
            throw new InvalidArgumentException(NOT_FOUND_MESSAGE);
        }
        return multiCriteria.get();
    }

    private double calculateNetPremium(double baseAmount, boolean hasAccidents, int driverAge) {
        double netPremium = baseAmount;

        if (driverAge < MIN_DRIVER_AGE) {
            netPremium *= DRIVER_AGE_COEFFICIENT;
        }

        if (hasAccidents) {
            netPremium *= ACCIDENT_COEFFICIENT;
        }

        return netPremium;
    }

    private double calculateTaxPremium(double netPremium) {
        return netPremium * TAX_COEFFICIENT;
    }

    private int calculateVehicleAge(Date registrationDate) {
        int vehicleAge = 0;

        Calendar present = Calendar.getInstance();
        Calendar past = Calendar.getInstance();
        past.setTime(registrationDate);

        while (past.before(present)) {
            past.add(Calendar.YEAR, 1);
            if (past.before(present)) {
                vehicleAge++;
            }
        }
        return vehicleAge;
    }

}
