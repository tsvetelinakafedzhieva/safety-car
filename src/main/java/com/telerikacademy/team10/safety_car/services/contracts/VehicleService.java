package com.telerikacademy.team10.safety_car.services.contracts;

import com.telerikacademy.team10.safety_car.models.VehicleBrand;
import com.telerikacademy.team10.safety_car.models.VehicleModel;

import java.util.Set;

public interface VehicleService {

    Set<VehicleBrand> getAllBrands();

    VehicleModel getModelById(long id);
}
