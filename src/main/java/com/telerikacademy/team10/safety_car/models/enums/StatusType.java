package com.telerikacademy.team10.safety_car.models.enums;

import com.telerikacademy.team10.safety_car.exceptions.InvalidArgumentException;
import org.springframework.lang.NonNull;

public enum StatusType {

    ACCEPTED,
    REJECTED,
    PENDING,
    CANCELED;

    private final static String ERROR_STATUS = "Invalid policy status";

    @NonNull
    public static StatusType statusTypeFromString(@NonNull String status) {

        switch (status.toUpperCase()) {
            case "ACCEPTED":
            case "ACCEPT":
                return StatusType.ACCEPTED;
            case "REJECTED":
            case "REJECT":
                return StatusType.REJECTED;
            case "CANCELED":
            case "CANCEL":
                return StatusType.CANCELED;
            case "PENDING":
                return StatusType.PENDING;
            default:
                throw new InvalidArgumentException(ERROR_STATUS);
        }
    }

}

