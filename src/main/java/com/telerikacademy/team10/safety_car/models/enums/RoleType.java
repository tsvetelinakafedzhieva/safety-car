package com.telerikacademy.team10.safety_car.models.enums;

public enum RoleType {
    ROLE_CUSTOMER, ROLE_AGENT
}
