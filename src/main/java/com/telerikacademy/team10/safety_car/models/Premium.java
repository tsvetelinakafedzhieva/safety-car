package com.telerikacademy.team10.safety_car.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Premium {

    private double totalPremium;
}
