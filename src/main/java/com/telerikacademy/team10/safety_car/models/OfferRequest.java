package com.telerikacademy.team10.safety_car.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class OfferRequest {

    private Date registrationDate;

    private int cubicCapacity;

    private int driverAge;

    private boolean hasPreviousAccidents;
}
