package com.telerikacademy.team10.safety_car.services;

import com.telerikacademy.team10.safety_car.exceptions.EntityNotFoundException;
import com.telerikacademy.team10.safety_car.exceptions.InvalidArgumentException;
import com.telerikacademy.team10.safety_car.exceptions.InvalidOperationException;
import com.telerikacademy.team10.safety_car.exceptions.UnauthorizedAccessException;
import com.telerikacademy.team10.safety_car.factories.PolicyFactory;
import com.telerikacademy.team10.safety_car.models.Customer;
import com.telerikacademy.team10.safety_car.models.Policy;
import com.telerikacademy.team10.safety_car.models.PolicyRequest;
import com.telerikacademy.team10.safety_car.models.enums.FilterType;
import com.telerikacademy.team10.safety_car.models.enums.StatusType;
import com.telerikacademy.team10.safety_car.repositories.contracts.CustomerRepository;
import com.telerikacademy.team10.safety_car.repositories.contracts.PolicyRepository;
import com.telerikacademy.team10.safety_car.services.contracts.AuthenticationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.Set;

import static org.mockito.Mockito.*;

class PolicyServiceImplTest {

    private final PolicyRepository policyRepository = mock(PolicyRepository.class);
    private final PolicyFactory policyFactory = mock(PolicyFactory.class);
    private final AuthenticationService authenticationService = mock(AuthenticationService.class);
    private final CustomerRepository customerRepository = mock(CustomerRepository.class);

    private final PolicyServiceImpl sut = new PolicyServiceImpl(
            policyRepository,
            policyFactory,
            authenticationService,
            customerRepository);

    @Test
    void createPolicy_Should_savePolicy_When_givenRequestPolicyIsValid() {
        PolicyRequest request = mock(PolicyRequest.class);
        String owner = "username@abv.bg";
        Policy policy = new Policy();
        when(policyFactory.create(request, owner)).thenReturn(policy);

        sut.createPolicy(request, owner);

        verify(policyRepository).save(policy);
    }

    @Test
    void getPoliciesForUser_Should_throwInvalidArgumentException_When_inputIsNotAgentOrCustomer() {
        String email = "username@abv.bg";
        when(authenticationService.isCustomer(email)).thenReturn(false);
        when(authenticationService.isAgent(email)).thenReturn(false);

        Assertions.assertThrows(InvalidArgumentException.class,
                () -> sut.getPoliciesForUser(email, Optional.empty(), Optional.empty()));
    }

    @Test
    void getPoliciesForUser_Should_returnUserPolicies_When_inputIsCustomer() {
        Set<Policy> expectedResult = mock(Set.class);
        String email = "username@abv.bg";
        Customer customer = new Customer();
        when(authenticationService.isCustomer(email)).thenReturn(true);
        when(customerRepository.findById(email)).thenReturn(Optional.of(customer));
        when(policyRepository.getAllPolicies(Optional.of(customer), FilterType.ANY, "")).thenReturn(expectedResult);

        Set<Policy> result = sut.getPoliciesForUser(email, Optional.empty(), Optional.empty());

        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    void getPoliciesForUser_Should_returnUserPolicies_When_inputIsAgent() {
        Set<Policy> expectedResult = mock(Set.class);
        String email = "username@abv.bg";
        when(authenticationService.isCustomer(email)).thenReturn(false);
        when(authenticationService.isAgent(email)).thenReturn(true);
        when(policyRepository.getAllPolicies(Optional.empty(), FilterType.NUMBER, "any")).thenReturn(expectedResult);

        Set<Policy> result = sut.getPoliciesForUser(email, Optional.of("NUMBER"), Optional.of("any"));

        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    void getPoliciesForUser_Should_throwInvalidArgumentException_When_typeIsEmptyButKeyIsPresent() {
        String email = "ivan@abv.bg";

        Assertions.assertThrows(InvalidArgumentException.class,
                () -> sut.getPoliciesForUser(email, Optional.of("string"), Optional.empty()));
    }

    @Test
    void getPoliciesForUser_Should_throwInvalidArgumentException_When_typeIsPresentButKeyIsEmpty() {
        String email = "ivan@abv.bg";

        Assertions.assertThrows(InvalidArgumentException.class,
                () -> sut.getPoliciesForUser(email, Optional.empty(), Optional.of("string")));
    }

    @Test
    void updatePolicyStatus_Should_throwUnauthorizedAccessException_When_userIsNotAgentAndStatusIsAccepted() {
        StatusType statusType = StatusType.ACCEPTED;
        long policyId = 6L;
        String username = "ivan@abv.bg";
        when(authenticationService.isAgent(username)).thenReturn(false);

        Assertions.assertThrows(UnauthorizedAccessException.class,
                () -> sut.updatePolicyStatus(policyId, username, statusType));
    }

    @Test
    void updatePolicyStatus_Should_throwUnauthorizedAccessException_When_userIsNotAgentAndStatusIsRejected() {
        StatusType statusType = StatusType.REJECTED;
        long policyId = 6L;
        String username = "ivan@abv.bg";
        when(authenticationService.isAgent(username)).thenReturn(false);

        Assertions.assertThrows(UnauthorizedAccessException.class,
                () -> sut.updatePolicyStatus(policyId, username, statusType));
    }

    @Test
    void updatePolicyStatus_Should_throwInvalidOperationException_When_theNewStatusIsPending() {
        StatusType statusType = StatusType.PENDING;
        long policyId = 6L;
        String username = "ivan@abv.bg";

        Assertions.assertThrows(InvalidOperationException.class,
                () -> sut.updatePolicyStatus(policyId, username, statusType));
    }

    @Test
    void updatePolicyStatus_Should_throwUnauthorizedAccessException_When_userIsNotCustomerAndStatusIsCanceled() {
        StatusType statusType = StatusType.CANCELED;
        long policyId = 6L;
        String username = "ivan@abv.bg";
        when(authenticationService.isCustomer(username)).thenReturn(false);

        Assertions.assertThrows(UnauthorizedAccessException.class,
                () -> sut.updatePolicyStatus(policyId, username, statusType));
    }

    @Test
    void updatePolicyStatus_Should_throwInvalidOperationException_When_thePolicyStatusIsNotPending() {
        StatusType statusType = StatusType.REJECTED;
        long policyId = 6L;
        String username = "ivan@abv.bg";
        Policy policy = new Policy();
        policy.setStatus(StatusType.ACCEPTED);
        when(authenticationService.isAgent(username)).thenReturn(true);
        when(policyRepository.findById(policyId)).thenReturn(Optional.of(policy));

        Assertions.assertThrows(InvalidOperationException.class,
                () -> sut.updatePolicyStatus(policyId, username, statusType));
    }


    @Test
    void updatePolicyStatus_Should_changeStatus_When_userIsCustomerAndStatusIsCanceled() {
        StatusType statusType = StatusType.CANCELED;
        long policyId = 6L;
        String username = "ivan@abv.bg";
        Policy policy = new Policy();
        policy.setStatus(StatusType.PENDING);
        when(authenticationService.isCustomer(username)).thenReturn(true);
        when(policyRepository.findById(policyId)).thenReturn(Optional.of(policy));

        sut.updatePolicyStatus(policyId, username, statusType);

        verify(policyRepository).save(policy);
    }

    @Test
    void updatePolicyStatus_Should_changeStatus_When_userIsAgentAndStatusIsAccepted() {
        StatusType statusType = StatusType.ACCEPTED;
        long policyId = 6L;
        String username = "ivan@abv.bg";
        Policy policy = new Policy();
        policy.setStatus(StatusType.PENDING);
        when(authenticationService.isAgent(username)).thenReturn(true);
        when(policyRepository.findById(policyId)).thenReturn(Optional.of(policy));

        sut.updatePolicyStatus(policyId, username, statusType);

        verify(policyRepository).save(policy);
    }

    @Test
    void updatePolicyStatus_Should_changeStatus_When_userIsAgentAndStatusIsRejected() {
        StatusType statusType = StatusType.REJECTED;
        long policyId = 6L;
        String username = "ivan@abv.bg";
        Policy policy = new Policy();
        policy.setStatus(StatusType.PENDING);
        when(authenticationService.isAgent(username)).thenReturn(true);
        when(policyRepository.findById(policyId)).thenReturn(Optional.of(policy));

        sut.updatePolicyStatus(policyId, username, statusType);

        verify(policyRepository).save(policy);
    }

    @Test
    void getById_Should_throwEntityNotFoundException_When_PolicyDoesNotExist() {
        long policyId = 6L;
        when(policyRepository.findById(policyId)).thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> sut.getById(policyId));
    }


    @Test
    void getById_Should_returnPolicy_When_PolicyExists() {
        long policyId = 6L;
        Policy expected = new Policy();
        when(policyRepository.findById(policyId)).thenReturn(Optional.of(expected));

        Policy result = sut.getById(policyId);

        Assertions.assertEquals(expected, result);
    }

    @Test
    void generatePolicyNumber_Should_returnTodaySlash5_When_customerHas4Policies() throws ParseException {
        LocalDate today = LocalDate.now();
        String prefix = today.format(DateTimeFormatter.ISO_DATE);
        String expectedPolicyNumber = String.format("%s/%d", prefix, 5);
        Set<Policy> customerPolicies = mock(Set.class);
        when(customerPolicies.size()).thenReturn(4);
        Customer customer = mock(Customer.class);
        when(customer.getPolicies()).thenReturn(customerPolicies);

        String result = sut.generatePolicyNumber(customer);

        Assertions.assertEquals(expectedPolicyNumber, result);
    }

}