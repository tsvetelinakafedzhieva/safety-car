package com.telerikacademy.team10.safety_car.services;

import com.telerikacademy.team10.safety_car.exceptions.EntityNotFoundException;
import com.telerikacademy.team10.safety_car.models.VehicleBrand;
import com.telerikacademy.team10.safety_car.models.VehicleModel;
import com.telerikacademy.team10.safety_car.repositories.contracts.VehicleBrandRepository;
import com.telerikacademy.team10.safety_car.repositories.contracts.VehicleModelRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.mockito.Mockito.*;

class VehicleServiceImplTest {

    VehicleModelRepository modelRepository = mock(VehicleModelRepository.class);

    VehicleBrandRepository brandRepository = mock(VehicleBrandRepository.class);

    VehicleServiceImpl sut = new VehicleServiceImpl(modelRepository, brandRepository);

    @Test
    void getAllBrands_Should_returnAllBrandsFromRepository() {
        Set<VehicleBrand> expected = new HashSet<>();
        when(brandRepository.findAll()).thenReturn(expected);

        Set<VehicleBrand> result = sut.getAllBrands();

        Assertions.assertEquals(expected, result);
    }


    @Test
    void getBrandModels_Should_throwEntityNotFoundException_When_modelFromRepositoryIsEmpty() {

        when(modelRepository.findById(anyLong())).thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> sut.getModelById(anyLong())
        );
    }

    @Test
    void getBrandModels_Should_returnModel_When_modelFromRepositoryHasValue() {
        long modelId = 12L;
        VehicleModel expected = new VehicleModel();
        when(modelRepository.findById(modelId)).thenReturn(Optional.of(expected));

        VehicleModel result = sut.getModelById(modelId);

        Assertions.assertEquals(expected, result);
    }



}