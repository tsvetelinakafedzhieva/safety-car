package com.telerikacademy.team10.safety_car.factories;

import com.telerikacademy.team10.safety_car.exceptions.InvalidArgumentException;
import com.telerikacademy.team10.safety_car.models.Customer;
import com.telerikacademy.team10.safety_car.models.Policy;
import com.telerikacademy.team10.safety_car.models.PolicyRequest;
import com.telerikacademy.team10.safety_car.models.VehicleModel;
import com.telerikacademy.team10.safety_car.models.enums.StatusType;
import com.telerikacademy.team10.safety_car.repositories.contracts.CustomerRepository;
import com.telerikacademy.team10.safety_car.repositories.contracts.VehicleModelRepository;
import com.telerikacademy.team10.safety_car.services.contracts.FileService;
import com.telerikacademy.team10.safety_car.utility.DateUtility;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class PolicyFactoryImplTest {

    private final VehicleModelRepository modelRepository = mock(VehicleModelRepository.class);
    private final CustomerRepository customerRepository = mock(CustomerRepository.class);
    private final FileService fileService = mock(FileService.class);


    PolicyFactory sut = new PolicyFactoryImpl(modelRepository, customerRepository, fileService);

    @Test
    void createPolicy_Should_throwInvalidArgumentException_When_givenModelIdIsInvalid() {
        PolicyRequest request = createPolicyRequest();
        String owner = "username@abv.bg";

        when(modelRepository.findById(request.getModelId())).thenReturn(Optional.empty());

        Assertions.assertThrows(InvalidArgumentException.class, () -> sut.create(request, owner));
    }

    @Test
    void createPolicy_Should_throwInvalidArgumentException_When_givenCustomerIdIsInvalid() {
        PolicyRequest request = createPolicyRequest();
        String owner = "username@abv.bg";
        VehicleModel model = new VehicleModel();

        when(modelRepository.findById(request.getModelId())).thenReturn(Optional.of(model));
        when(customerRepository.findById(owner)).thenReturn(Optional.empty());

        Assertions.assertThrows(InvalidArgumentException.class, () -> sut.create(request, owner));
    }

    @Test
    void create_Should_createNewPolicy_When_allParametersAreValid() {
        String ownerId = "ivan@abv.bg";
        PolicyRequest request = createPolicyRequest();
        VehicleModel model = mock(VehicleModel.class);
        when(modelRepository.findById(request.getModelId())).thenReturn(Optional.of(model));
        Customer owner = mock(Customer.class);
        when(customerRepository.findById(ownerId)).thenReturn(Optional.of(owner));
        String registrationCertificateLocation = "/file/certificates";
        when(fileService.storeImage(request.getRegistrationCertificate())).thenReturn(registrationCertificateLocation);

        Policy expected = Policy.builder()
                .policyNumber(request.getPolicyNumber())
                .owner(owner)
                .effectiveDate(request.getEffectiveDate())
                .model(model)
                .registrationDate(request.getRegistrationDate())
                .cubicCapacity(request.getCubicCapacity())
                .hasPreviousAccidents(request.getHasPreviousAccidents())
                .registrationCertificate(registrationCertificateLocation)
                .driverAge(request.getDriverAge())
                .totalAmount(request.getTotalAmount())
                .status(StatusType.PENDING)
                .email(request.getEmail())
                .phone(request.getPhone())
                .city(request.getCity())
                .street(request.getStreet())
                .creationDate(DateUtility.convertToDate(LocalDate.now()))
                .build();

        Policy result = sut.create(request, ownerId);

        Assertions.assertEquals(expected,result);
    }


    private PolicyRequest createPolicyRequest() {
        MultipartFile registrationCertificate = mock(MultipartFile.class);
        PolicyRequest request = mock(PolicyRequest.class);
        when(request.getModelId()).thenReturn(2L);
        when(request.getRegistrationCertificate()).thenReturn(registrationCertificate);
        when(request.getEffectiveDate()).thenReturn(DateUtility.convertToDate(LocalDate.now().plusDays(1)));
        when(request.getRegistrationDate()).thenReturn(DateUtility.convertToDate(LocalDate.now().minusYears(12)));
        when(request.getCubicCapacity()).thenReturn(1600);
        when(request.getHasPreviousAccidents()).thenReturn(true);
        when(request.getDriverAge()).thenReturn(27);
        when(request.getTotalAmount()).thenReturn(956.23);
        when(request.getEmail()).thenReturn("username@abv.bg");
        when(request.getPhone()).thenReturn("+3598886325411");
        when(request.getCity()).thenReturn("Sofia");
        when(request.getStreet()).thenReturn("12A");
        when(request.getPolicyNumber()).thenReturn("4526398712369");
        return request;
    }



}